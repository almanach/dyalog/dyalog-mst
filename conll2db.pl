#!/usr/bin/env perl

use strict;
use AppConfig qw/:argcount :expand/;
use Text::Scan;
use List::Util qw{max min};
use Text::LevenshteinXS qw(distance);

my $config = AppConfig->new(
			    "oracle!" => {DEFAULT => 0},
			    "lattice!" => {DEFAULT => 0},
			    "graph!" => {DEFAULT => 0},
			    "proj!" => {DEFAULT => 0},
			    "tagset=f",
			    "mode=s" => {DEFAULT => 'gold'},
			    "random_skip=d" => {DEFAULT => 0 }
			   );

$config->args();

my $oracle_seg = [];

my $oracle = $config->oracle();
my $lattice = $config->lattice();
my $graph = $config->graph();	# sagae style data
my $proj = $config->proj();	# projective or not
my $late_attach=0;
my $tagset = $config->tagset();
my $mode = $config->mode();
my $skip = $config->random_skip();

my $mstag_fset=0;

my $lexer_info = {
		  xfullcat => []
		 };

if ($tagset && -f $tagset) {
  open(TAGSET,"<",$tagset) || die "can't read tagset $tagset: $!";
  my $print = 0;
  while(<TAGSET>) {
    if (/^:-finite_set/) {
      # deactivate finite set for mstag
      #      /mstag/ and $mstag_fset=1;
      $print = 1;
    }
    $print and print $_;
    if ($print and /\)\./) {
      print "\n";
      $print = 0;
    }
    if (s/^%%\s+lexer\s*:\s*//o) {
      chomp;
      # lexer directives
      if (s/^xfullcat\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{xfullcat} = \@f;
	next;
      }

      if ($mode eq 'pred' && /^compounds\s+(\S+)/) {
	my $scan = $lexer_info->{compounds} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if ($mode eq 'pred' && /^dict\s+(\S+)/) {
	my $scan = $lexer_info->{dict} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (/^subcat\s+(\S+)/) {
	my $scan = $lexer_info->{subcat} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (s/^split\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{split}{$_} = 1 foreach (@f);
      }

      if (s/^mstag_complete\s*//o) {
	$lexer_info->{mstag_complete} = 1;
      }

      if (s/^mstag_list\s*//o) {
	$lexer_info->{mstag_list} = 1;
      }

      if (s/^extra\s+(\S+)//o) {
	$lexer_info->{extra}{$1} = 1;
      }

      if (s/^mstag_filter\s+'(\S+)'//o) {
	$lexer_info->{mstag_filter}{$1} = 1;
      }

      if (s/^mstag_exclude_features\s+//o) {
	foreach my $f (split(/\s+/,$_)) {
	  $lexer_info->{mstag_exclude_feature}{$f} = 1;
	}
      }

      if (s/^cluster\s+(\S+)$//o) {
	my $cluster = $1;
#	print "loading cluster file $cluster\n";
	my $scan = $lexer_info->{cluster} = Text::Scan->new;
	$scan->restore($cluster);
#	print "done loading\n";
	$lexer_info->{collect_sentence} = 1;
      }
      
      if (s/^align\s+(\S+)$//o) {
	$lexer_info->{align} = $1;
      }

      if (s/^delta_reverse\s+//o){
	foreach my $f (split(/\s+/,$_)) {
	  $lexer_info->{delta_reverse}{$f} = 1;
	}
      }

      if (s/^gold\s+//o) {
	# use gold dependencies as features, but forget some of them
	$lexer_info->{gold} = [split(/\s+/,$_)];
      }

    }
  }
  close(TAGSET);
}


my $sid=1;
my $sentid;
my $max=0;
my $tree={};
my $segmap = {};
my $root;
my $rootlabel;
my $count=0;
my @dag = ();
my $path = {};
my $maxlattice;
my $ids={};
my $memoactions={};

my $sinfo = {};

my $nfields;
my $oracle_id=0;

while(<>) {
  chomp;
  if (/^##\s*shuffle\s+(\d+)/) {
    $oracle_id = $1;
    next;
  }
  /^#/ and next;		# comments
  if (/^\s*$/) {
    # white lines: sentence separators

    if ($skip && int(rand($skip))) {
      print "%% Skipping sentence E$sid\n\n";
      goto RESET;
    }

FLUSH:
    if (exists $lexer_info->{collect_sentence}) {
      my $sentence = '';
      my $pos2elt = {};
      foreach my $elt (@dag) {
	my ($left,$form) = @{$elt->{data}};
	$pos2elt->{length($sentence)} = $elt;
	$sentence .= "$form ";
      }

      if (my $scanner = $lexer_info->{compounds}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($mwe,$pos,$cat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $left = $elt->{data}[0]-1;
	  ## print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $elt->{data}[5] =~ /mwehead=/ or $elt->{data}[5] .= "|mwehead=${cat}+";
	  foreach my $w (split(/\s+/,$mwe)) {
	    my $elt = $pos2elt->{$pos};
	    $elt->{data}[5] =~ /pred=y/ or $elt->{data}[5] .= "|pred=y";
	    $pos += 1 + length($w);
	  }
	}
      }

      if (my $scanner = $lexer_info->{dict}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cats) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $cats = join(',',map {quote($_)} grep {$_} split(/\|/,$cats));
	  $elt->{dict} = $cats;
	}
      }

      if (my $scanner = $lexer_info->{subcat}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$subcat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
##	  $subcat = join(',',map {quote($_)} grep {$_} split(/\|/,$subcat));
	  $elt->{subcat} = $subcat;
	}
      }

      if (my $scanner = $lexer_info->{cluster}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cid) = @$occ;
##	  print "cluster $dform $cid in <$sentence>\n";
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  $elt->{cluster} = $cid;
	}
      }


    }

    if (keys %$path) {
      # lattice mode: check dead ends
      my $alive = {$maxlattice => 1};
      keep_alive(0,$path,$alive);
      @dag = grep {$alive->{$_->{right}}} @dag;
    }

    if (@$oracle_seg) {
      (defined $oracle_id) or $oracle_id = $sid-1;
##      my $current_seg = shift @$oracle_seg;
      my $current_seg = $oracle_seg->[$oracle_id];
      undef $oracle_id;
      foreach my $entry (@$current_seg) {
	if ($entry->[7] eq 'aux_pass') {
	  $current_seg->[$entry->[6]-1][5] .= "|diathesis=passive";
	} elsif ($entry->[7] eq 'aux_tps') {
	  $current_seg->[$entry->[6]-1][5] .= "|diathesis=active";
	}
      }
#      foreach my $e (@dag) {
#	my $d = $e->{data};
#	print "%% list cand: $d->[0] $d->[1] $d->[4]\n";
#      }
#      print "%% using oracle seg\n";
      my $seen = {};
      my @stack = ([1,0]);
      while (@stack) {
	my $info = shift @stack;
	my $current_pos = $info->[0];
	exists $seen->{$info->[0]}{$info->[1]} and next;
	$seen->{$info->[0]}{$info->[1]} = 1;
	my $current_tok = $current_seg->[$info->[1]];
	$current_tok or next;
	print "%% search candidates at pos $current_pos: oracle lex = $current_tok->[1] deplabel=$current_tok->[7]\n";
	my @candidates = grep {$_->{data}[0] == $current_pos} @dag;
	my $lex = $current_tok->[1];
	my $lemma = $current_tok->[2];
	my $cat = $current_tok->[3];
	my $fullcat = $current_tok->[4];
	my $mstag = $current_tok->[5];
	my $deplabel = $current_tok->[7];
##	$lex =~ s/\@\S+$//o;					   # @suff is Hebrew data !
	foreach my $candidate (@candidates) {
##	  print "%% list2 cand: $candidate->{data}[0] $candidate->{data}[1] $candidate->{data}[4]\n";
	  my $cost = 1;
	  if ($lexer_info->{align} eq 'hebrew') {
	    ## print "%% Hebrew align\n";
	    ## align for Hebrew (no lemma, cat=fulltag)
	    $lex =~ s/\@\S+$//o;
	    ($lex eq $candidate->{data}[1]) or next;
	    if ($cat eq $candidate->{data}[3]
		&& $mstag eq $candidate->{data}[5]) {
	      $cost = 16;
	    } elsif ($cat eq $candidate->{data}[3]) {
	      $cost = 8;
	    } else {
	      $cost = 2;
	    }
	    $cost > 1 or next;
	  } else {
	    ## align for French (default)
	    my $d = distance($lex,$candidate->{data}[1]);
	    ($d < 4) or next;
	    my $cmstag = $candidate->{data}[5];
	    my $ccat = $candidate->{data}[4];
	    ($candidate->{data}[1] eq $lex) and $cost *= (1 + 1 / (1+$d)); #lex
	    $candidate->{data}[2] eq $current_tok->[2] and $cost *= 1.2; # lemma
	    $candidate->{data}[3] eq $current_tok->[3] and $cost *= 2; # cat
	    ($fullcat eq 'VINF' && $cmstag =~ /mode=infinitive/o) and $cost *= 1.2;
	    ($fullcat eq 'VIMP' && $cmstag =~ /mode=imperative/o) and $cost *= 1.2;
	    ($fullcat eq 'VPR' && $cmstag =~ /mode=gerundive/o) and $cost *= 1.2;
	    ($fullcat eq 'VPP' && $cmstag =~ /mode=participle/o) and $cost *= 1.2;
	    ($fullcat eq 'VS' && $cmstag =~ /mode=subjontive/o) and $cost *= 1.2;
	    ($fullcat eq 'CS' &&  $ccat eq 'csu') and $cost *= 1.2;
	    ($fullcat eq 'CC' &&  $ccat eq 'coo') and $cost *= 1.2;
	    ($fullcat eq 'PROREL' &&  $ccat eq 'prel') and $cost *= 1.2;
	    ($fullcat eq 'PROWH' &&  $ccat eq 'pri') and $cost *= 2;
	    ($cat eq 'PRO' &&  $deplabel eq 'suj' && $cmstag =~ /case=nom/o) and $cost *= 1.2;
	    ($cat eq 'PRO' &&  $deplabel eq 'obj' && $cmstag =~ /case=acc/o) and $cost *= 1.2;
	    ($cat eq 'CL' &&  $deplabel eq 'suj' && $cmstag =~ /case=nom/o) and $cost *= 1.2;
	    ($cat eq 'CL' &&  $deplabel eq 'obj' && $cmstag =~ /case=acc/o) and $cost *= 1.2;
	    ($lemma eq 'que' && $fullcat eq 'ADV' && $ccat eq 'que') and $cost *= 2;
	    ($mstag =~ /n=s/o && $cmstag =~ /number=sg/o) and $cost *= 1.2;
	    ($mstag =~ /n=p/o && $cmstag =~ /number=pl/o) and $cost *= 1.2;
	    ($mstag =~ /p=([123])/o && $cmstag =~ /number=$1/o) and $cost *= 1.2;
	    ($mstag =~ /g=m/o && $cmstag =~ /gender=masc/o) and $cost *= 1.2;
	    ($mstag =~ /g=f/o && $cmstag =~ /gender=fem/o) and $cost *= 1.2;
	    ($mstag =~ /m=ind/o && $cmstag =~ /mode=indicative/o) and $cost *= 1.2;
	    ($mstag =~ /t=cond/o && $cmstag =~ /mode=conditional/o) and $cost *= 1.2;
	    ($mstag =~ /t=pst/o && $cmstag =~ /tense=present/o) and $cost *= 1.2;
	    ($mstag =~ /diathesis=active/ && $cmstag =~ /diathesis=active/) and $cost *= 1.2;
	    ($mstag =~ /diathesis=passive/ && $cmstag =~ /diathesis=passive/) and $cost *= 1.2;
	    ($deplabel =~ /^aux_(tps|pass)/o && $ccat eq 'aux' ) and $cost *= 2;
	    # $candidate->{data}[4] eq $current_tok->[4] and $cost *= 2; # fullcat
	    # $candidate->{data}[5] eq $current_tok->[5] and $cost *= 2; # mstag
	  }
	  (!exists $candidate->{segcost} || $cost > $candidate->{segcost}) or next;
	  $candidate->{segcost} = $cost;
	  my $right = $info->[1];
	  $right < $maxlattice or next;
	  push(@stack,[$candidate->{right}+1,$right+1]);
	}
      }
      my $max = max map {find_best_path($_,\@dag)} grep {exists $_->{segcost}} @dag;
      print "%% best path max is $max\n";
      my @first = grep {($_->{data}[0] == 1) 
			  && exists $_->{segbest}
			    && $_->{segbest} == $max
			  } @dag;
      my $first = $first[0];
      while ($first && @$current_seg) {
	my $current_tok = shift @$current_seg;
	my $left = $first->{data}[0]-1;
	my $right = $first->{right};
	print "%% align $left $right $first->{data}[1] vs $current_tok->[1] $current_tok->[3] $current_tok->[4]: $first->{data}[3] $first->{data}[4] $first->{data}[5]\n";
	$first->{segalign} = $current_tok;
	exists $first->{segbest} or last;
	$first = $first->{segnext};
      }
      $first and print "%% *** bad align: missing gold tokens\n";
      @$current_seg and print "%% *** bad align: missing lattice tokens\n";
      ($first || @$current_seg) and print "oracle!skip_sentence.\n\n";
    }

    elt_process($_) foreach (@dag);

    my $maxleft = 0;
    foreach my $left (sort {$a <=> $b} keys %$sinfo) {
      $sinfo->{$left}{size} = $sinfo->{$left}{n} * ($sinfo->{$left+1}{n} || 1) * ($sinfo->{$left+2}{n} || 1);
      $maxleft = $left;
    }

    foreach my $step (0 .. 2 * $maxleft) {
      my $x = 1;
      my $k = int($step/2);
      my $n = 0;
      foreach my $i (($k-2) .. ($k+2)) {
	($i < 0 || $i > $maxleft || !exists $sinfo->{$i}) and next;
	$sinfo->{$i}{size} > $x and $x = $sinfo->{$i}{size};
	$n ++;
      }
      my $avg = int(log($x));
      $avg < 1 and $avg = 1;
      print <<EOF;
'B2'($step,$avg). %% k=$k n=$n x=$x
EOF
    }

    foreach my $left (sort {$a <=> $b} keys %$sinfo) {
      my $entry = $sinfo->{$left};
      my $qform = $entry->{form};
      $qform or next;
      my $size = int(log($entry->{size} || 1));
      $size ||= 1;
      my @cats = sort {$a cmp $b} keys %{$entry->{cat}};
      @cats > 1 and push(@cats,join("_",@cats));
      my $cats = join(',',map {quote($_)} @cats);
      print <<EOF;
'L'($left,$qform,[$cats]).
'B'($left,$size).
EOF
      if (exists $entry->{pred_gov}) {
	my $gov = $entry->{pred_gov};
	my $label = quote($entry->{label} || "none");
	push(@{$sinfo->{$gov}{dep}},$left);
	print <<EOF;
guide!head($left,$gov).
guide!label($left,$label).
EOF
      }

      if (exists $entry->{multi_pred_gov}) {
	foreach my $gov (keys %{$entry->{multi_pred_gov}}) {
	  foreach  my $label (keys %{$entry->{multi_pred_gov}{$gov}}) {
	    my $qlabel =  quote($label);
	    push(@{$sinfo->{$gov}{dep}},$left);
	    print <<EOF;
guide!head($left,$gov,$qlabel).
EOF
	  }
	}
      }
    }
    
      foreach my $left (sort {$a <=> $b}
			grep {exists $sinfo->{$_}{pred_gov}}
			keys %$sinfo) {
      my $entry = $sinfo->{$left};
      my $gov =  $sinfo->{$left}{pred_gov};
      $gov < $left or next;
      my $label = quote($sinfo->{$left}{label});
      my $rightmost = $left;
      while (exists $sinfo->{$rightmost}{dep}) {
	my $tmp = $sinfo->{$rightmost}{dep}[-1];
	$tmp > $rightmost or last;
	$rightmost = $tmp;
      }
      $rightmost++;
      print <<EOF;
guide!action($gov,$left,$rightmost,reduce_right,$label).
EOF
    }

    foreach my $left (sort {$a <=> $b} grep {exists $sinfo->{$_}{multi_pred_gov}} keys %$sinfo) {
      my $entry = $sinfo->{$left};
      foreach my $gov (keys %{$sinfo->{$left}{multi_pred_gov}}) {
	$gov < $left or next;
	foreach my $label (keys %{$sinfo->{$left}{multi_pred_gov}{$gov}}) {
	  my $qlabel = quote($label);
	  my $rightmost = $left;
	  while (exists $sinfo->{$rightmost}{dep}) {
	    my $tmp = $sinfo->{$rightmost}{dep}[-1];
	    $tmp > $rightmost or last;
	    $rightmost = $tmp;
	  }
	  $rightmost++;
	  print <<EOF;
guide!action($gov,$left,$rightmost,reduce_right,$qlabel).
EOF
	}
      }
    }
    
    
    foreach my $i (0..3) {
  print <<EOF;
'D'($i,
    lemma{ lex => $i, 
           lemma => $i, 
           cat => $i, 
           mstag => '_',
           fullcat => $i,
           xfullcat => $i,
           dict => [],
           length => 0
         }
).

EOF

    }

    print <<EOF;

'N'($max).
'S'('E$sid').
%% line=$.

EOF

    print <<EOF;

%% SENTENCE DIV %%
%%%EOF

EOF

  RESET:
    $sid++;
    undef $sentid;
    $max=0;
    $tree ={};
    $segmap = {};
    $root=undef;
    $rootlabel=undef;
    $count = 0;
    @dag = ();
    $path = {};
    $maxlattice = 0;
    $ids={};
    $memoactions = {};
    $sinfo = {};
    next;			# done sentence
  }
  my @elt = split(/\t/,$_);
  # conllu schema for contracted words
  $elt[0] =~ /^\d+-\d+/ and next;
  my $tok;
  my $right;
  if ($lattice) {
    my $left = shift(@elt);
    $right = shift(@elt);
    $path->{$left}{$right} ||= 1;
    $right > $maxlattice and $maxlattice = $right;
    $left++;
##    $right++;
    unshift(@elt,$left);
    $tok = pop(@elt);
  }
  $nfields ||= scalar(@elt);
  if ( scalar(@elt) != $nfields 
       && $elt[5] =~ /^\d+/
     ) {
    warn "pb in CONLL file mismatch mstag/head: @elt\n";
    splice(@elt,5,0,'_');
  }
  my $entry = { data => [@elt], dict => '' };
  $tok and $entry->{tok} = $tok;
  $right and $entry->{right} = $right;
  push(@dag,$entry);
  eof() and goto FLUSH;		# this case used when no empty line at the end of the file !
}


if ($oracle) {
  print <<EOF;

oracle!end.

%% SENTENCE DIV %%
%%%EOF

EOF
}

sub elt_process {
  my ($elt) = @_;
  my ($left,$lex,$lemma,$cat,$fullcat,$mstag,@other) = @{$elt->{data}};
  if ($graph && exists $ids->{$left}) {
    # multi line in graph format
    if ($oracle) {
      my $id = $left;
      $segmap->{$id}=$id;
      if (@other) {
	my ($head,$label) = @other;
	$label = quote($label);
	$head < 0 or $tree->{$head}{$id}{$label} = 1;
      }
    }
    return; 
  }
  my $xfullcat = $fullcat;
  $left--;
  $max = (exists $elt->{right}) ? $elt->{right} : ($left+1);
  $count++;
  my $fs = "'_'";
  $mstag =~ /atbpos/ and $mstag = Morpho::Arabic::handle($mstag);
  if ($cat eq 'V' && $elt->{subcat}) {
    my $subcat = $elt->{subcat};
    my @mstag = ($mstag,$subcat);
    $subcat !~ /\bObj=/ and push(@mstag,'Obj=none');
    $subcat !~ /\bObjde=/ and push(@mstag,'Objde=none');
    $subcat !~ /\bObj�=/ and push(@mstag,'Obj�=none');
    $subcat !~ /\bAtt=/ and push(@mstag,'Att=none');
    $mstag = join('|',@mstag);
  }
  (exists $elt->{cluster}) and $mstag .= "|cluster=$elt->{cluster}";
  if ($mstag =~ /\|/ || $lexer_info->{mstag_list}) {
    my @fs = split(/\|/,$mstag);
    if ($mstag =~ /sentid=(\d+)/){
      $sentid = $1;
      print <<EOF;
sentence_id('sentid=$sentid').
EOF
      @fs = grep {!/^sentid=/} @fs;
    }
    if (exists $lexer_info->{mstag_filter}) {
      @fs = grep {exists $lexer_info->{mstag_filter}{$_}} @fs;
    }
    if (exists $lexer_info->{mstag_exclude_feature}) {
      @fs = grep {/^(\S+)=/ && !exists $lexer_info->{mstag_exclude_feature}{$1}} @fs;
    }
    ## *** TMP: remove mate_head feature
    @fs = grep {!/^(mate|mate_sem)_head=/} @fs;
    if (@{$lexer_info->{xfullcat}}) {
      my %fs = (map {my @u = split(/=/,$_,2); $u[0] => $u[1]} grep {/.=./} @fs);
      foreach my $f (grep {exists $fs{$_}} @{$lexer_info->{xfullcat}}) {
	$xfullcat .= $fs{$f};
	delete $fs{$f};
      }
      @fs = grep {my @u=split(/=/,$_,2); exists $fs{$u[0]}} @fs;
    }
    $lexer_info->{mstag_complete} and push(@fs,$mstag);
    if (1) {
      if (my @delta = grep {$_ =~ /^frmg_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	defined $lexer_info->{delta_reverse}{frmg} and $delta = -$delta;
	$sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^gold_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	$sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^spmrl_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	# use guiding info if none alredady present
	# ($delta && !exists $sinfo->{$left}{pred_gov}) and $sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^(mate|mate_sem)_delta=/} @fs) {
	my ($type,$delta) = $delta[0] =~ /(mate|mate_sem)_delta=(\S+)/;
	defined $lexer_info->{delta_reverse}{$type} and $delta = -$delta;
	($left-$delta > 0) and $sinfo->{$left}{pred_gov}= $left-$delta;
      }
      my %ogre = ();
      foreach my $fv (grep {/^ogre_(?:\S+?)=/} @fs) {
	my ($f,$v) = split(/=/,$fv);
	my ($iter,$rule) = $f =~ /^ogre_(\d+)_(\S+)/;
	my @v = split(/,/,$v);
	my $delta = $v[-1];
	if ($left-$delta > 0) {
#	  $sinfo->{$left}{pred_gov} = $left-$delta;
	  #	  $sinfo->{$left}{label} = $v[-2];
	  #	  print "%% process ogre $left $delta $fv\n";
	  if (1) {
	    my $entry = $sinfo->{$left}{multi_pred_gov} ||= {};
	    $entry->{$left-$delta}{"$iter:$v[-2]"} = 1;
	  }
	}
	my $action = 'rename';
	if ($v[-2] =~ /^_->/) {
	  $action = 'add';
	} elsif ($v[-2] =~ /->_$/) {
	  $action = 'del';
	}
	if (1) {
	  foreach my $new (
#			   "delta_$f=$v[-1]",
#			   "label_$f=$v[-2]",
			   #			   "action_$f=$action",
			   "rule_ogre_$iter=$rule",
			   "delta_ogre_$iter=$v[-1]",
			   "label_ogre_$iter=$v[-2]",
			   "action_ogre_$iter=$action"
			  ) {
	    $ogre{$new}=1;
	  }
	}
      }
      @fs = grep {!/^ogre_/} @fs;
      if (keys %ogre) {
	push(@fs,keys %ogre);
      }
      if (@fs && @fs==1) {
	$fs = quote($fs[0]);
      } elsif (@fs) {
	$fs = join(',',  map {quote($_)} grep {$_} @fs );
	$fs = "[$fs]";
	$mstag_fset and $fs = "mstag$fs";
      } else {
	$fs = "'_'"
      }
    }
    # get label info for guiding
    # - favor gold label if present
    # - other labels otherwise
    # should add some kind of sorting info between multiple sources of guiding
    # (or add the functionality to have multiple guides in dyalog-sr)
    if (my @label = grep {$_ =~ /^gold_label=/} @fs) {
      my ($label) = $label[0] =~ /=(\S+)/;
      $sinfo->{$left}{label}= $label;
    }
    if (my @label = grep {$_ =~ /^(\w+)_label=/ && $1 ne 'spmrl'} @fs) {
      my ($label) = $label[0] =~ /=(\S+)/;
      (exists $sinfo->{$left}{label}) or $sinfo->{$left}{label}= $label;
    }

  } else {
    if (@{$lexer_info->{xfullcat}} && $mstag =~ /(\S+)=(\S+)/) {
      my ($f,$v) = ($1,$2);
      if (grep {$f eq $_} @{$lexer_info->{xfullcat}}) {
	$xfullcat .= $v;
	$mstag = '_';
      }
    }
    $fs = quote($mstag || '_');
  }
  if ($lexer_info->{extra}{number} && $lex =~ /^\d+(?:[.,]\d*)?/) {
    $xfullcat .= "_num";
  }
  if ($lexer_info->{extra}{capitalize} && $lex =~ /^[A-Z������������]/o) {
    $xfullcat .= "_cap";
  }
  my ($qlex,$qlemma,$qcat,$qfullcat,$qxfullcat) = map {quote($_)} ($lex,$lemma,$cat,$fullcat,$xfullcat);
  exists $lexer_info->{split}{cat} and $qcat = qsplit($cat);
  exists $lexer_info->{split}{fullcat} and $qfullcat = qsplit($fullcat);
  exists $lexer_info->{split}{xfullcat} and $qxfullcat = qsplit_pos($xfullcat);
  my $dinfo = $elt->{dict};
  my $length = $max - $left;
  my $id = $count;
  exists $elt->{tok} and $id .= "+$elt->{tok}";
  print <<EOF;
'C'($left,
    $id,
    lemma{ lex => $qlex, 
           lemma => $qlemma, 
           cat => $qcat, 
           mstag => $fs,
           fullcat => $qfullcat,
           xfullcat => $qxfullcat,
           dict => [$dinfo],
           length => $length
         },
    $max).

EOF

  $ids->{$id} = $length;

  $sinfo->{$left}{form} = $qlex;
  $sinfo->{$left}{cat}{$cat} = 1;
  $sinfo->{$left}{n}++;

  my $conllmax = $max;

   if (exists $elt->{segalign}) {
    my $oracle_tok = $elt->{segalign};
    @other = @{$oracle_tok}[6,7];
    $conllmax = $oracle_tok->[0];
    print "%% getting from oracle token: @other\n";
  }
  
  if ($oracle && (@other || $graph)) {
    $segmap->{$conllmax}=$id;
    if (@other) {
      my ($head,$label) = @other;
      $label = quote($label);
      unless ($head < 0) {
	$tree->{$head}{$conllmax}{$label} = 1;
	print <<EOF;
oracle!edge($head,$id,$label).

EOF
      }
    }
  }

}

sub quote {
  my $string = shift;
  return $string if ($string =~ /^\d+$/ && !$string =~ /^0\d+/);
##  return $string if ($string =~ /^[a-z]\w*$/o);
  return $string if ($string =~ /^[a-z][a-zA-Z��������������������������������������]*$/o);
  $string =~ s/\'/\'\'/og;
  # $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}

sub qsplit {
  my $s = shift;
  ($s =~ /^P\+/) and return quote($s);
  my @s = map {quote($_)} split(/\+/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}

sub qsplit_pos {
  my $s = shift;
  my $i = 1;
  ($s =~ /^P\+/) and return quote($s);
  my @s = map {quote($i++.":$_")} split(/[+_]/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}



######################################################################
package Morpho::Arabic;

## inspired from script add_arabic_morph_features_labels.pl
## by authors : Djam� Seddah and Miguel Ballesteros
## distributed in SPMRL 2013

## to be moved in some external module

our $map;

sub init {
  $map or
    $map = {
	    'ADJ.VN' => 'subcat=vn',
	    'ADJ.COMP' => 'subcat=comp',
	    'ADJ.NUM' => 'subcat=num',
	    'ADJ.NUM' => 'subcat=num',
	    'CASE_DEF_ACC' => 'case=acc|casedefinitness=y',
	    'CASE_DEF_GEN' => 'case=gen|casedefinitness=y',
	    'CASE_DEF_NOM' => 'case=nom|casedefinitness=y',
	    'CASE_INDEF_ACC' => 'case=acc|casedefinitness=n',
	    'CASE_INDEF_GEN' => 'case=gen|casedefinitness=n',
	    'CASE_INDEF_NOM' => 'case=nom|casedefinitness=n',
	    'CONNEC_PART' => 'subcat=connec',
	    'CV' => 'aspect=imperative|aspect=imperative',
	    'CVSUFF_DO:1p' => 'catsuff=CVSUFF|aspect=imperative||func=do||pers=1|number=p',
	    'CVSUFF_DO:1'  => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=1|number=s',
	    'CVSUFF_DO:3F' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=f|number=s',
	    'CVSUFF_DO:3MP' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=p',
	    'CVSUFF_DO:3M' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=s',
	    'CVSUFF_SUBJ:2F' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=f|number=s',
	    'CVSUFF_SUBJ:2MP' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=p',
	    'CVSUFF_SUBJ:2M' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=s',
	    'DEM_PRON_FD' => 'gender=f|number=d',
	    'DEM_PRON_F' => 'gender=f|number=s',
	    'DEM_PRON_MD' => 'gender=m|number=d',
	    'DEM_PRON_MP' => 'gender=m|number=p',
	    'DEM_PRON_P' => 'number=p',
	    'DET_ADJ.VN' => 'subcat=vn',
	    'DET_NOUN_VN' => 'subcat=vn',
	    'DET_NOUN_NUM' => 'subcat=num',
	    'DET_NOUN_PROP' => 'subcat=prop',
	    'DET_NOUN_QUANT' => 'subcat=quant',
	    'EMPHATIC_PART' => 'subcat=emphatic',
	    'FOCUS_PART' => 'subcat=focus',
	    'FUT_PART' => 'subcat=fut',
	    'INTERROG_ADV' => 'subcat=interrog',
	    'INTERROG_PART' => 'subcat=interrog',
	    'INTERROG_PRON' => 'subcat=interrog',
	    'IV1P' => 'aspect=imperfect|pers=1|number=p',
	    'IV1' => 'aspect=imperfect|pers=1|number=s',
	    'IV2D' => 'aspect=imperfect|pers=2|number=d',
	    'IV2FP' => 'aspect=imperfect|pers=2|gender=f|number=p',
	    'IV2F' => 'aspect=imperfect|pers=2|gender=f|number=s',
	    'IV2MP' => 'aspect=imperfect|pers=2|gender=m|number=p',
	    'IV2M' => 'aspect=imperfect|pers=2|gender=m|number=s',
	    'IV3FD' => 'aspect=imperfect|pers=3|gender=f|number=d',
	    'IV3MD' => 'aspect=imperfect|pers=3|gender=m|number=d',
	    'IV3MP' => 'aspect=imperfect|pers=3|gender=m|number=p',
	    'IV3M' => 'aspect=imperfect|pers=3|gender=m|number=s',
	    'IVSUFF_DO:1P' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=p',
	    'IVSUFF_DO:1' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=s',
	    'IVSUFF_DO:2F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=f|number=s',
	    'IVSUFF_DO:2MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=p',
	    'IVSUFF_DO:2M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=s',
	    'IVSUFF_DO:3D' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|number=d',
	    'IVSUFF_DO:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=f|number=s',
	    'IVSUFF_DO:3MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=p',
	    'IVSUFF_DO:3M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=s',
	    'IVSUFF_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|mood=i',
	    'IVSUFF_MOOD:J' => 'catsuff=IVSUFF|aspect=imperfect|mood=j',
	    'IVSUFF_MOOD:' => 'catsuff=IVSUFF|aspect=imperfect|mood=s',
	    'IVSUFF_SUBJ:2FS_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=i',
	    'IVSUFF_SUBJ:2FS_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=sj',
	    'IVSUFF_SUBJ:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=3|gender=f|number=s',
	    'IVSUFF_SUBJ:D_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=i',
	    'IVSUFF_SUBJ:D_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=sj',
	    'IV_PAS' => 'iv=pass',
	    'IV' => 'aspect=imperfect',
	    'JUS_PART' => 'subcat=JU',
	    'NEG_PART' => 'subcat=NEG',
	    'NOUN.VN' => 'subcat=vn',
	    'NOUN_NUM' => 'subcat=num',
	    'NOUN_PROP' => 'subcat=prop',
	    'NOUN_QUANT' => 'subcat=quant',
	    'NSUFF_FEM_DU_ACC' => 'catsuff=NSUFF|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=accgen',
	    'NSUFF_FEM_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_NOM' => 'catsuff=NSUFF|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_PL' => 'catsuff=NSUFF|gender=fem|number=p',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_M_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_MASC_DU_ACC' => 'catsuff=NSUFF|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_NOM' => 'catsuff=NSUFF|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_PL' => 'catsuff=NSUFF|gender=masc|number=p',
	    'NSUFF_MASC_PL_ACC' => 'catsuff=NSUFF|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACCGEN_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACC_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_GEN' => 'catsuff=NSUFF|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_NOM' => 'catsuff=NSUFF|gender=masc|number=p|case=nom',
	    'NSUFF_MASC_PL_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=nom',
	    'POSS_PRON1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'POSS_PRON_1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON_1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON_2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON_2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON_2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON_2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON_2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON_3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON_3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON_3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON_3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON_3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'PRON1P' => 'pers=1|number=p',
	    'PRON1S' => 'pers=1|number=s',
	    'PRON2FP' => 'pers=2|gender=f|number=p',
	    'PRON2FS' => 'pers=2|gender=f|number=s',
	    'PRON2MP' => 'pers=2|gender=m|number=p',
	    'PRON2MS' => 'pers=2|gender=m|number=s',
	    'PRON3D' => 'pers=3|number=d',
	    'PRON3FP' => 'pers=3|gender=f|number=p',
	    'PRON3FS' => 'pers=3|gender=f|number=s',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON_1P' => 'pers=1|number=p',
	    'PRON_1S' => 'pers=1|number=s',
	    'PRON_2FP' => 'pers=2|gender=f|number=p',
	    'PRON_2FS' => 'pers=2|gender=f|number=s',
	    'PRON_2MP' => 'pers=2|gender=m|number=p',
	    'PRON_2MS' => 'pers=2|gender=m|number=s',
	    'PRON_3D' => 'pers=3|number=d',
	    'PRON_3FP' => 'pers=3|gender=f|number=p',
	    'PRON_3FS' => 'pers=3|gender=f|number=s',
	    'PRON_3MP' => 'pers=3|gender=m|number=p',
	    'PRON_3MS' => 'pers=3|gender=m|number=s',
	    'PVSUFF|pers=3|M' => 'catsuff=PVSUFF|aspect=perfect|pers=3|gender=m|number=s',
	    'PVSUFF_DO:1P' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=p',
	    'PVSUFF_DO:1S' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=s',
	    'PVSUFF_DO:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=f|number=s',
	    'PVSUFF_DO:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=p',
	    'PVSUFF_DO:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=s',
	    'PVSUFF_DO:3D' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|number=d',
	    'PVSUFF_DO:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=f|number=s',
	    'PVSUFF_DO:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:1P' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=p',
	    'PVSUFF_SUBJ:1S' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=s',
	    'PVSUFF_SUBJ:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=f|number=s',
	    'PVSUFF_SUBJ:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=p',
	    'PVSUFF_SUBJ:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=s',
	    'PVSUFF_SUBJ:3FD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=d',
	    'PVSUFF_SUBJ:3FP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=p',
	    'PVSUFF_SUBJ:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=s',
	    'PVSUFF_SUBJ:3MD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=d',
	    'PVSUFF_SUBJ:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:3MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=s',
	    'PV_PAS' => 'voice=pasive',
	    'RC_PART' => 'subcat=RC',
	    'RESTRIC_PART' => 'subcat=RESTRIC',
	    'VERB_PART' => 'subcat=VERB',
	    'VOC_PART' => 'subcat=VOC',
	   };
}

sub morpho2conll {
  my $s = shift;
  $s =~ s/^DET\+/DET_/o;
  return join('|', grep {$_} map {$map->{$_}} split(/\+/,$s));
}

sub handle {
  my $s = shift;
  init;
  $s =~ s/atbpos=([^\s\|]+)/&morpho2conll($1)/e;
  $s =~ s/^\|//o;
#  print "converted to <$s>\n";
  return $s;
}

1;
