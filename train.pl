#!/usr/bin/env perl

## training script

use strict;
use POSIX qw(strftime);
use AppConfig;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
			    'train=f' => {DEFAULT => "ftb6_1.conll"},
			    'dev=f' => {DEFAULT => "ftb6_2.conll"},
                            'cdir=f' => { DEFAULT => "/Users/clergeri/Work/Corpus/FTB6/" },
                            'iter=d' => {DEFAULT => 10},
			    'beam=d' => {DEFAULT => 1},
			    'model=s' => {DEFAULT => 'ftb6.model'},
			    'evalcmd=f' => {DEFAULT => "/Users/clergeri/Work/corpus_proc/conll_eval.pl"},
			    'log=f' => { DEFAULT => "train.log" },
			    'mdir=s' => {DEFAULT => 'models'},
			    'tagset=s' => {DEFAULT => 'ftb.tagset.db'},
			    'mode=s' => {DEFAULT => 'early'}, # early, late, best
			    'lattice=s',
			    'spmrl=s',
			    'test=s',
			    'malt_home=s' => {DEFAULT => $ENV{MALT_HOME}},
			    'malt_jar=s' => {DEFAULT => 'maltparser-1.7.1.jar'},
			    'proj!' => {DEFAULT => 0}, # projectivize,
			    'extra_lexer_opts=s' => {DEFAULT => ''},
			    'shuffle!' => {DEFAULT => 0},
			    'binary!' => {DEFAULT => 0}
			   );

$config->args;

my $model_base=$config->model;
my $cdir = $config->cdir;
my $evalcmd = $config->evalcmd;
my $train = $config->train;
my $dev = $config->dev;
my $test = $config->test;
my $beam = $config->beam;
my $maxiter = $config->iter;
my $tagset = $config->tagset;
my $mode = $config->mode;
my $extra_lexer_opts = $config->extra_lexer_opts;

my $sbeam = "-sbeam ".$config->sbeam;
#my $sbeam = "";

my $binary = $config->binary ? '_binary' : '';

my $proj = $config->proj;
my $malt_home = $config->malt_home;
my $malt_jar = $config->malt_jar;

my $spmrl = $config->spmrl;
my $lattice_flag = $config->lattice;

my $shuffle = '';

if ($config->shuffle) {
    $shuffle = '| ./conll_shuffle.pl';
}

my $tedeval_opts = "";

if (defined $spmrl) {
  my ($lang,$mode,$lattice) = split(/_/,$spmrl);
  $lang =~ s/5k// and $train = '5k';
  my $tagset_base = "spmrl_${lang}";
  $proj and $tagset_base .= "_proj";
  $tagset = "${tagset_base}.tagset.db";
  -f "${tagset_base}_${mode}.tagset.db"
    and $tagset = "${tagset_base}_${mode}.tagset.db";
  ($lang eq 'swedish' or $lang eq 'hebrew') and $train = '5k';
  my $ulang = uc($lang);
  $lang eq 'swedish' or $lang = ucfirst($lang);
  $mode ||= 'gold';
  my $suff = "$lang.$mode.conll";
  ($mode eq 'pred' && $lang eq 'French') and $suff = "$lang.pred_jk.conll";
  if (defined $lattice && $lattice =~ /lattice/) {
    $lattice_flag = ($lattice eq 'lattice.nodisamb' && $mode eq 'pred') ? 'nodisamb' : 'disamb';
    ## $suff .= '.tobeparsed.tagged.lattices';
    $suff .= '.tobeparsed';
    if ($mode eq 'gold') {
      $suff .= ".gold_tagged+gold_token.lattices";
    } else {
      $suff .= ".pred_tagged+pred_token.${lattice_flag}.lattices";
    }
    if ($lattice_flag eq 'nodisamb') {
      my $tmplang = lc($lang);
      $tagset = "${tagset_base}_lattice.tagset.db";
      $tedeval_opts .= " --lang $tmplang --malt_home ${malt_home}";
    }
  }
  ($train eq '5k') or $train = '';
  $train = "train$train.$suff";
  $dev = "dev.$suff";
  $dev =~ s/_jk//;
  $cdir .= "/${ulang}_SPMRL";
  $cdir .= "/$mode/conll";
#  print "spmrl lang=$lang cdir=$cdir train=$train dev=$dev\n";
#  exit;
}

-f $tagset
  or die "missing tagset file '$tagset': $!";

my $mdir = $config->mdir;

-d $mdir 
  or mkdir $mdir;

-d $mdir
  or die "couldn't build model directory '$mdir': $!";

my $hostname = `hostname`;
chomp $hostname;

my $logfile = $config->log;
open(LOG,">$mdir/$logfile") || die "can't open logfile '$mdir/$logfile': $!";

verbose("training hostname=$hostname iter=$maxiter train=$train dev=$dev mode=$mode beam=$beam model=$model_base mdir=$mdir");

my $gtrain = $spmrl ? ($train =~ /5k/ ? "$cdir/train5k/$train" : "$cdir/train/$train") : "$cdir/$train";
my $gdev = $spmrl ? "$cdir/dev/$dev" : "$cdir/$dev";
my $gtest;

$test and $gtest = "$cdir/$test";

my $dounproj = "";

if ($proj) {
  my $pproj = "pproj.$$.${beam}";
  my $doproj = "java -jar ${malt_home}/${malt_jar} -c $pproj -m proj -pp head -i $gtrain -o $mdir/train.proj.conll";
  system("$doproj") == 0
    or die "system failed: $?";
  $gtrain = "$mdir/train.proj.conll";
  $dounproj = "| java -jar ${malt_home}/${malt_jar} -c $pproj -m deproj -i /dev/stdin -o /dev/stdout";
}

foreach my $iter (1..$maxiter) {
  verbose("iteration $iter");
  my $prev = $iter-1;
  my $suff = "b${beam}i${iter}";
  my $model = "$mdir/$model_base.$suff";
  my $ldev = "$mdir/$dev.$suff";
  my $eval = "$mdir/$dev.eval.$suff";
  my $in = $prev ? "-load${binary}_model $mdir/$model_base.b${beam}i${prev}" : "";
  my $out = "-save${binary}_model $model";
  my $lexer_opts = "--tagset $tagset $extra_lexer_opts";
  ($spmrl && $spmrl =~ /_pred/) and $lexer_opts .= " --mode pred";
  my $train_lexer_opts = $lexer_opts;
  $train_lexer_opts .= " -oracle ";
  if ($lattice_flag) {
    my $goldseg = $gtrain;
    $goldseg =~ s/\.conll.*$/.conll/o;
    $train_lexer_opts .= " --oracle_seg=$goldseg --lattice";
    print "LATTICE MODE train_lexer_opts=$train_lexer_opts\n";
  }
  verbose("\tbuilding $model on $train");
  my $cmd = "cat $gtrain | yarecode -l=fr -u $shuffle | ./conll2db.pl $train_lexer_opts 2> $mdir/lexer_errs.log | tee $mdir/lexer.log |./mst_parser -loop -utime -- -train -res $tagset $in $out -multi 2> $mdir/parser_err.log > $mdir/$train.answer.$suff";
#  print STDERR "$cmd\n";
  (-f "$model") or
    system("$cmd") == 0
      or die "system failed: $?";
  verbose("\trunning on dev $dev");
  $lattice_flag and $lexer_opts .= " --lattice ";
  (-f $ldev) or
    system("cat $gdev| yarecode -l=fr -u |./conll2db.pl $lexer_opts | ./mst_parser -loop -utime -- -res $tagset -load${binary}_model $model -multi | ./answer2conll.pl $dounproj |  yadecode -l=fr -u > $ldev") == 0
       or die "system failed: $?" ;
  if ($lattice_flag) {
    my $evalcmd = "cat $ldev | ./tedeval.pl $tedeval_opts -o $eval.ted";
    print STDERR "tedeval: $evalcmd\n";
    system($evalcmd) == 0
      or die "system failed: $?";
    my $stat = `grep "^AVG:" $eval.ted`;
    chomp $stat;
    my ($score) = $stat =~ /^AVG:\s+\S+\s+(\S+)/;
    $score *= 100;
    verbose("\tmodel=$model LAS=$score");
  } else {
    system("$evalcmd -g $gdev -s $ldev 2> /dev/null > $eval") == 0
      or die "system failed: $?";
    my $stat = `grep "Labeled   attachment score" $eval`;
    chomp $stat;
    $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAS=$1");
  }
  if ($gtest) {
    my $ltest = "$mdir/$test.$suff";
    my $eval = "$mdir/$test.eval.$suff";
    (-f $ltest) or
      system("cat $gtest| yarecode -l=fr -u |./conll2db.pl $lexer_opts | ./mst_parser -loop -utime -- -res $tagset -load${binary}_model $model -multi | ./answer2conll.pl $dounproj |  yadecode -l=fr -u > $ltest") == 0
	or die "system failed: $?" ;
    system("$evalcmd -g $gtest -s $ltest 2> /dev/null > $eval") == 0
      or die "system failed: $?";
    my $stat = `grep "Labeled   attachment score" $eval`;
    chomp $stat;
    $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAStest=$1");
  }
}

verbose("done");

sub verbose {
    my $x = shift;
    my $date = strftime "[%F %H:%M:%S]", localtime;
    my $msg = "$date : $x\n";
    print LOG $msg;
    print $msg;
}

close(LOG);
