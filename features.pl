/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  features.pl -- Handling features and feature templates
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').

:-extensional 'N'/1, 'C'/4, 'S'/1,'D'/2,'L'/3,'rC'/4, guide!head/2, guide!label/2.
:-features( lemma, [lex,lemma,xfullcat,fullcat,cat,mstag,dict,length] ).
:-finite_set(ponct,[',','.',';','(',')','[',']','!','?',':','"']).

:-extensional tagset!label/1.
:-extensional tagset!templates/1, tagset!mst!templates/1.
:-extensional tagset!fullcat/2.
:-extensional tagset!cat/2.
:-extensional tagset!template_prefix/1.
:-extensional tagset!complete.
:-extensional tagset!outside_bound/1.

:-extensional templates/3.
:-extensional templates/2.

:-xcompiler
null_entry(Entry,V) :- Entry = lemma{ lex => V, lemma => V, cat => V, fullcat => V, xfullcat => V, dict => [], length => 0, mstag => '_'}.	

:-xcompiler
simple_label(L,XL) :-
	( tagset!simple_label(L,XL) -> true ; L=XL)
	.

:-xcompiler
feature_strip(V,XV) :-
    (V=[XV] xor V=XV)
	.

:-xcompiler
get_word_features(I,Id,EntryI::lemma{}) :-
%    format('try word features I=~w Id=~w\n',[I,Id]),
    ( (I < 0 ; Id ==0) ->
              Id = 0,
	      null_entry(EntryI,Id)
	  ; 'N'(N), I >= N ->
		V is I-N,
		Id = 0,
		('D'(V,EntryI) xor null_entry(EntryI,0))
	  ;
	  'C'(I,Id,EntryI::lemma{},_) ->
	  true
	  ;
	  fail
	),
%	format('=> ~w\n',[EntryI]),
	true
	.

:-xcompiler
get_xword_features(I,Form,AllCats) :-
    ( 'L'(I,Form,AllCats) xor Form=0,AllCats=0)
	.


:-std_prolog discrete_distance/2.

discrete_distance(D,XD) :-
    (D > 0 ->
	 ( D < 8 -> XD = D
	  ; D < 10 -> XD = 8
	  ; D < 14 -> XD = 10
	  ; D < 20 -> XD = 14
	  ; XD = 20
	 )
     ;
     ( D > -8 -> XD = D
      ; D > -10 -> XD = -8
      ; D > -14 -> XD = -10
      ; D > -20 -> XD = -14
      ; XD = -20
     )
    )
.

:-light_tabular ponct_features/3.
:-mode(ponct_features/3,+(+,+,-)).

ponct_features(Left,Right,XFeats) :-
    ( Right is Left + 1 -> 
	  XFeats = [nocomma,noponct]
      ;
      mutable(M,[],true),
      XLeft is Left + 1,
      XRight is Right - 1,
      every((
		   term_range(XLeft,XRight,_TLeft),
		   domain(_Left,_TLeft),
		   ('C'(_Left,_,lemma{ lex => _Lex::ponct[] }, _ ) xor fail),
		   mutable_read(M,_Feats),
		   ( _Lex = ',' ->
			 ( domain('comma',_Feats) xor mutable_list_extend(M,comma) ),
			 ( \+ domain('mcoma',_Feats),
			   domain(_XLeft,_TLeft),
			   _XLeft > _Left,
			   'C'(_XLeft,_,lemma{ lex => ','}, _ ) ->
			       mutable_list_extend(M,mcomma)
			   ;
			   fail
			 )
		     ; _Lex = ponct['(','['] ->
			   ( domain('open',_Feats) xor mutable_list_extend(M,open) )
		     ; _Lex = ponct[']',')'] ->
			   ( domain('close',_Feats) xor mutable_list_extend(M,close) )
		     ; _Lex = ponct['.','!','?',':',';'] ->
			   ( domain('final',_Feats) xor mutable_list_extend(M,final) )
		     ; \+ domain(ponct,_Feats),
		       mutable_list_extend(M,ponct)
		   )		     
	   )),
      mutable_read(M,Feats1),
      ( domain('comma',Feats1) xor mutable_list_extend(M,nocoma) ),
      ( Feats1=[] xor mutable_list_extend(M,noponct) ),
      mutable_read(M,Feats),
      feature_strip(Feats,XFeats),
      verbose('ponct features ~w ~w => ~w\n',[Left,Right,XFeats]),
      true
    )
.


%:-light_tabular edge_features/5.
%:-mode(edge_features/5,+(+,+,+,+,-)).

:-std_prolog edge_features/5.
	
%% out: Feat0 Feat1 EntryI EntryI2 EntryI3 PonctFeatsI1 Guide
edge_features(SLeft,SId,TLeft,TId,
	      Features::[ Dir,
			  Delta,
			  SFeatures,
			  TFeatures,
			  PonctFeats,
			  Inside,
			  LOutside,
			  ROutside,
			  Guide
			]
	     ) :-
    _Delta is TLeft - SLeft,
    discrete_distance(_Delta,Delta),
    (SId == 0 ->
	 %% root node
	 null_entry(RootEntry,0),
	 SFeatures = [SLeft,
		      RootEntry,
		      RootEntry,
		      RootEntry,
		      RootEntry,
		      RootEntry
		     ]
    ;
    node_features(SLeft,SId,SFeatures)
    ),
    node_features(TLeft,TId,TFeatures),
    (SId = 0 ->
	 Dir = root,
	 Inside = none,
	 LOutside = none,
	 ROutside = none,
	 PonctFeats = none
    ; SLeft < TLeft ->
      Dir = right,
      ponct_features(SLeft,TLeft,PonctFeats),
      ('C'(SLeft,_,_,SRight) xor fail),
      inside_features(SRight,TLeft,Inside),
      outside_features(SRight,TLeft,LOutside,ROutside)
    ; SLeft > TLeft ->
      Dir = left,
      ponct_features(TLeft,SLeft,PonctFeats),
      ('C'(TLeft,_,_,TRight) xor fail),
      inside_features(TRight,SLeft,Inside),
      outside_features(TRight,SLeft,LOutside,ROutside)
    ;
    Dir = none,
    PonctFeats = none,
    Inside = none,
    LOutside = none,
    ROutside = none
    ),
    ( guide!head(TLeft,SLeft) ->
      ( guide!label(TLeft,Label) ->
	guide!action(reduce,Label,Guide)
      ;
      Guide = 'reduce'
      )
    ; guide!label(TLeft,Label) ->
      guide!action(wreduce,Label,Guide)
    ;
    Guide = 'none'
    ),
    true
.

:-xcompiler
guide!action(Action,Label,Guide) :-
    (recorded(guide!xaction(Action,Label,Guide)) ->
	 true
    ;
    name_builder('~w_~w',[Action,Label],Guide),
    record(guide!xaction(Action,Label,Guide))
    )
    .

:-light_tabular node_features/3.
:-mode(node_features/3,+(+,+,-)).

node_features(Left,Id,
	      Features::[
		  Left,
		  Entry,
		  EntryB1,
		  EntryB2,
		  EntryA1,
		  EntryA2
	      ]) :-
    get_word_features(Left,Id,Entry::lemma{}),
    ('C'(Left,Id,_,LeftA1) xor LeftA1 is Left+1),
    get_word_features(LeftA1,IdA1,EntryA1::lemma{}),
    ('C'(LeftA1,IdA1,_,LeftA2) xor LeftA2 is LeftA1+1),
    get_word_features(LeftA2,IdA2,EntryA2::lemma{}),
    ('rC'(Left,IdB1,_,LeftB1) xor LeftB1 is Left-1),
    get_word_features(LeftB1,IdB1,EntryB1::lemma{}),
    ('rC'(LeftB1,IdB2,_,LeftB2) xor LeftB2 is LeftB1-1),
    get_word_features(LeftB2,IdB2,EntryB2::lemma{}),
    true
.

:-std_prolog inside_features/3.

inside_features(Left,Right,Features) :-
    mutable(MF,[]),
    every(( fcat_at(Right,FCat,VR),
	    (fcat_at(Left,FCat,VL) xor VL=0),
	    V is VR-VL,
	    V > 0,
	    guide!action(FCat,V,U),
	    mutable_list_extend(MF,U)
	  )),
    mutable_read(MF,Features),
%    format('inside feature ~w ~w => ~w\n',[Left,Right,Features]),
    true
.

:-std_prolog outside_features/4.

outside_features(Left,Right,LFeatures,RFeatures) :-
    mutable(MLF,[]),
    mutable(MRF,[]),
    'N'(N),
    (tagset!outside_bound(Bound) xor Bound = 5),
    XLeft is max(0,Left-Bound),
    XRight is min(N,Right+Bound),
    every(( fcat_at(XRight,FCat,VXR),
	    (fcat_at(Right,FCat,VR) xor VR=0),
	    (fcat_at(Left,FCat,VL) xor VL=0),
	    (fcat_at(XLeft,FCat,VXL) xor VXL=0),
	    VLeft is VL-VXL,
	    (VLeft > 0 ->
		 guide!action(FCat,VLeft,ULeft),
		 mutable_list_extend(MLF,ULeft)
	    ;
	    true
	    ),	    
	    VRight is VRR-VR,
	    (VRight > 0 ->
		 guide!action(FCat,VRight,URight),
		 mutable_list_extend(MRF,URight)
	    ;
	    true
	    ),
	    true
	  )),
    mutable_read(MLF,LFeatures),
    mutable_read(MRF,RFeatures),
%    format('outside feature ~w ~w => ~w\n',[Left,Right,Features]),
    true
.


:-light_tabular build_fcat_at/1.
:-mode(build_fcat_at/1,+(+)).

:-extensional fcat_at/3.

build_fcat_at(Pos) :-
    ( 'N'(Pos) -> true
    ; 'C'(Pos,_,lemma{},NPos) ->
      every((
		   every((
				fcat_at(Pos,_FCat,N),
				(
				    ('C'(Pos,_,lemma{ xfullcat => XFCat },NPos),
				     (XFCat = [_|_] -> domain(FCat,XFCat) ; FCat = XFCat ),
				     _FCat = FCat
				    )
				->
				    M is N+1
				;
				M = N
				),
				record_without_doublon(fcat_at(NPos,_FCat,M))
			    )),
		   'C'(Pos,_,lemma{ xfullcat => XFCat },NPos),
		   (XFCat = [_|_] -> domain(FCat,XFCat) ; FCat = XFCat ),
		   \+ fcat_at(NPos,FCat,_),
		   record(fcat_at(NPos,FCat,1))
	       )),
      build_fcat_at(NPos)
    ;
    fail
    )
.
    
%% Data: the list of all features
%% Should go in some resource file
:-extensional feature_map/2.

feature_map( [ Dir,
	       Delta,
	       [LeftS,
		lemma{ lex => LexS, lemma => LemmaS, cat => CatS, xfullcat => FullCatS, mstag => MstagS, dict => DictS, length => LS},
		lemma{ lex => BLexS, lemma => BLemmaS, cat => BCatS, xfullcat => BFullCatS, dict => BDictS, length => BLS},
		lemma{ lex => B2LexS, lemma => B2LemmaS, cat => B2CatS, xfullcat => B2FullCatS, dict => B2DictS, length => B2LS},
		lemma{ lex => ALexS, lemma => ALemmaS, cat => ACatS, xfullcat => AFullCatS, dict => ADictS, length => ALS},
		lemma{ lex => A2LexS, lemma => A2LemmaS, cat => A2CatS, xfullcat => A2FullCatS, dict => A2DictS, length => A2LS}
	       ],
	       [ LeftT,
		 lemma{ lex => LexT, lemma => LemmaT, cat => CatT, xfullcat => FullCatT, mstag => MstagT, dict => DictT, length => LT},
		 lemma{ lex => BLexT, lemma => BLemmaT, cat => BCatT, xfullcat => BFullCatT, dict => BDictT, length => BLT},
		 lemma{ lex => B2LexT, lemma => B2LemmaT, cat => B2CatT, xfullcat => B2FullCatT, dict => B2DictT, length => B2LT},
		 lemma{ lex => ALexT, lemma => ALemmaT, cat => ACatT, xfullcat => AFullCatT, dict => ADictT, length => ALT},
		 lemma{ lex => A2LexT, lemma => A2LemmaT, cat => A2CatT, xfullcat => A2FullCatT, dict => A2DictT, length => A2LT}
	       ],
	       PonctFeats,
	       Inside,
	       LOutside,
	       ROutside,
	       Guide
	     ],
	     [ dir: Dir,
	       delta: Delta,
	       
	       lexS: LexS,	% *S: source features
	       lemmaS: LemmaS,
	       catS: CatS,
	       fullcatS: FullCatS,
	       dictS: DictS,
	       lS: LS,
	       
	       lexT: LexT,	% *T: target features
	       lemmaT: LemmaT,
	       catT: CatT,
	       fullcatT: FullCatT,
	       dictT: DictT,
	       lT: LT,
	       
	       blexS: BLexS,
	       blemmaS: BLemmaS,
	       bcatS: BCatS,
	       bfullcatS: BFullCatS,
	       bdictS: BDictS,
	       blS: BLS,
	       
	       alexS: ALexS,
	       alemmaS: ALemmaS,
	       acatS: ACatS,
	       afullcatS: AFullCatS,
	       adictS: ADictS,
	       alS: ALS,
	       
	       blexT: BLexT,
	       blemmaT: BLemmaT,
	       bcatT: BCatT,
	       bfullcatT: BFullCatT,
	       bdictT: BDictT,
	       blT: BLT,
	       
	       alexT: ALexT,
	       alemmaT: ALemmaT,
	       acatT: ACatT,
	       afullcatT: AFullCatT,
	       adictT: ADictT,
	       alT: ALT,

	       b2lexS: B2LexS,
	       b2lemmaS: B2LemmaS,
	       b2catS: B2CatS,
	       b2fullcatS: B2FullCatS,
	       b2dictS: B2DictS,
	       b2lS: B2LS,
	       a2lexS: A2LexS,
	       a2lemmaS: A2LemmaS,
	       a2catS: A2CatS,
	       a2fullcatS: A2FullCatS,
	       a2dictS: A2DictS,
	       a2lS: A2LS,

	       b2lexT: B2LexT,
	       b2lemmaT: B2LemmaT,
	       b2catT: B2CatT,
	       b2fullcatT: B2FullCatT,
	       b2dictT: B2DictT,
	       b2lT: B2LT,
	       a2lexT: A2LexT,
	       a2lemmaT: A2LemmaT,
	       a2catT: A2CatT,
	       a2fullcatT: A2FullCatT,
	       a2dictT: A2DictT,
	       a2lT: A2LT,

	       mstagS: MstagS,
	       mstagT: MstagT,

	       ponct: PonctFeats,
	       guide: Guide,
	       inside: Inside,
	       loutside: LOutside,
	       routside: ROutside
	     
	     ]
	    )
.

:-std_prolog expand_flist_disj/4.

expand_flist_disj(FNames,Map,Values,FSet) :-
	( FNames = [FNames1|FNames2] ->
	  expand_flist(FNames1,Map,Values1,FSet),
	  expand_flist_disj(FNames2,Map,_Values2,FSet),
	  (_Values2 =.. [disj|Values2] -> true ; Values2 = _Values2),
	  (Values1 =.. [disj|_Values1] ->
	       append(_Values1,Values2,Values)
	   ;
	   Values = [Values1|Values2]
	  )
	;
	  Values = []
	)
	.

:-std_prolog append/3.

append(A,B,C) :-
    (A = [] -> C = B
     ; A = [X|A2],
       C = [X|C2],
       append(A2,B,C2)
    )
.

:-std_prolog expand_flist/4.

expand_flist(FNames,Map,Values,FSet) :-
	( FNames = true ->
	  (expand_to_symbol(FSet,Smb) xor format('*** could not expand smb ~w\n',[FSet]), fail),
	  verbose('template=~w\n',[Smb]),
	  (domain(label:Label,Map) ->
	       Values = [Smb,Label]	% first path with label
	  ;
	  Values = [Smb]	% second path without label: to deal with attachment
	  )
	; FNames = [_|_] ->
	  (expand_flist_disj(FNames,Map,Values1,FSet) xor format('*** could not expand flist ~w\n',[FNames]), fail),
	  ( Values1 = [Values]
	   -> true
	   ; Values1 =.. [disj|_] ->
		 Values = Values1
	  ;
	    Values =.. [disj|Values1]
	  )
	; FNames = (F:FNames2) ->
	      (expand_flist(FNames2,Map,Values2,[F|FSet]) xor format('*** could not expand flist2 ~w\n',[FNames2]), fail),
	      ( domain(F:V,Map) ->
		    (recorded(feature2index(F,Index)) xor
			     update_counter(feature_ctr,Index),
		     record(feature2index(F,Index))
		    ),
%%		    Values = [(Index,V)|Values2]
	      	    Values = [V|Values2]
	  ;
	  format('*** warning: feature ~w not found in map\n',[F]),
	  Values = Values2
	   % fail
	  )
	;
	  format('*** bad format ~w\n',[FNames]),
	  fail
	)
	.

:-std_prolog expand_to_symbol/2.

expand_to_symbol(FNames,Smb) :-
	( FNames = [Smb] -> true
	; FNames = [F|FNames2],
	  expand_to_symbol(FNames2,Smb2),
	  name_builder('~w_~w',[Smb2,F],Smb)
	)
	.

:-std_prolog template_add/4.

template_add(F,T,Templates,XTemplates)	:-
%	format('try add F=~w T=~w to ~w\n',[F,T,Templates]),
	( F = (F1,F2) ->
	  template_add(F1,T,Templates,XTemplates1),
	  template_add(F2,T,XTemplates1,XTemplates)
	; Templates = [] ->
	  XTemplates = [F:XT],
	  template_factorize([T],XT)
	; Templates = [true|Templates2] ->
	  template_add(F,T,Templates2,XTemplates2),
	  XTemplates = [true|XTemplates2]
	; Templates = [F1:T1|Templates2],
	  ( F = F1 ->
	    ( T = true ->
	      ( domain(T,T1) ->
		XTemplates = Templates
	      ;
		XTemplates = [F1:[true|T1]|Templates2]
	      )
	    ;
	      ( T=XF:XT ->
		template_add(XF,XT,T1,XT1)
	      ;
		template_add(T,true,T1,XT1)
	      ),
	      XTemplates=[F1:XT1|Templates2]
	    )
	  ;
	    template_add(F,T,Templates2,XTemplates2),
	    XTemplates = [F1:T1|XTemplates2]
	  )
	),
%	format('added F=~w T=~w to ~w => ~w\n',[F,T,Templates,XTemplates]),
	true
	.

:-std_prolog template_factorize/2.

template_factorize(Templates,XTemplates) :-
%	format('try factorize ~w\n',[Templates]),
	( Templates = [T|Templates2] ->
	  template_factorize(Templates2,XTemplates2),
	  ( T=true ->
	    ( domain(true,XTemplates2) -> XTemplates = XTemplates2
	    ; XTemplates = [true|XTemplates2]
	    )
	  ; T = F:T2 ->
	    template_add(F,T2,XTemplates2,XTemplates)
	  ;
	    template_add(T,true,XTemplates2,XTemplates)
	  )
	; Templates = [],
	  XTemplates = []
	),
%	format('factorized into ~w\n',[XTemplates]),
	true
	.

:-std_prolog template_analyze/1.

template_analyze(T) :-
	( T= true -> true
	; T=F:T2 ->
	  update_counter(feature(F),_),
	  template_analyze(T2)
	;
	  update_counter(feature(T),_)
	)
	.

:-std_prolog template_complete_add/3.

template_complete_add(F,TmpTemplates,CTemplates) :-
	( TmpTemplates = [] -> CTemplates = []
	; TmpTemplates = [T|TmpTemplates2] ->
	  template_complete_add(F,TmpTemplates2,CTemplates2),
	  CTemplates = [F:T,T|CTemplates2]
	)
	.
		      
:-std_prolog template_complete/2.

template_complete(T,CTemplates) :-
	( T=true -> CTemplates = [true]
	; T = F1:T2 ->
	  template_complete(T2,CTemplates2),
	  template_complete_add(F1,CTemplates2,CTemplates) 
	;
	  CTemplates = [T]
	)
	.

:-std_prolog template_add/3.

template_add(Add,Templates,XTemplates) :-
	( Add = [] -> XTemplates = Templates
	; Add = [T1|Add2] ->
	  template_add(Add2,Templates,XTemplates2),
	  (domain(T1,XTemplates2) -> XTemplates = XTemplates2 ; XTemplates = [T1|XTemplates2])
	)
	.

:-std_prolog template_reorder/2.

template_reorder(Templates,OTemplates) :-
	(Templates = [] -> OTemplates = []
	; Templates = [T|Templates2],
	 template_reorder(Templates2,OTemplates2),
	 template_reorder_aux(T,OT),
	 template_complete(OT,CTemplates),
	 template_add(CTemplates,OTemplates2,OTemplates)
	).

:-std_prolog template_reorder_aux/2.

template_reorder_aux(T,OT) :-
	( T=true -> OT=true
	; T=F:T2 ->
	  template_reorder_aux(T2,OT2),
	  template_reorder_add(F,OT2,OT),
	  true
	;
	  OT = T
	)
	.

:-std_prolog template_reorder_add/3.

template_reorder_add(F,OT1,OT2) :-
	( OT1 = true -> OT2 = F
	; OT1 = F1:XOT1 ->
	  value_counter(feature(F),FC),
	  value_counter(feature(F1),F1C),
	  ( FC > F1C ->
	    OT2 = F:OT1
	  ; template_reorder_add(F,XOT1,XOT2),
	    OT2 = F1:XOT2
	  )
	; OT1 = F1,
	  value_counter(feature(F),FC),
	  value_counter(feature(F1),F1C),
	  ( FC > F1C ->
	    OT2 = F:F1
	  ; 
	    OT2 = F1:F
	  )
	)
	.
	  
:-std_prolog generate_templates.

generate_templates :-
	feature_map(Feats,Map),
	verbose('handling templates\n',[]),
	(tagset!mst!templates(Templates)
	 xor tagset!templates(Templates)
	 xor format('*** No feature templates found\n',[]),
	 Templates = []
	),
	length(Templates,NTemplates),
	verbose('compiling #templates=~w\n',[NTemplates]),
	(tagset!complete -> 
	 every(( domain(_Template,Templates),
		 template_analyze(_Template)
	       )),
	 template_reorder(Templates,OTemplates)
	;
	 OTemplates = Templates
	),
	%%	format('try factorize template ~w\n',[OTemplates]),
	template_factorize(OTemplates,_XTemplates),
	mutable(M,_XTemplates,true),
	every(( tagset!template_prefix(Prefix),
		mutable_read(M,_XT),
		mutable(M,[Prefix: _XT])
	      )),
	mutable_read(M,XTemplates),
	verbose('factorized templates ~w\n',[XTemplates]),
	expand_flist(XTemplates,[label:Label|Map],Values,[]),
	verbose('expand tree template ~w => values=~w\n',[Templates,Values]),
	record_without_doublon(templates(Feats,Label,Values)),
	expand_flist(XTemplates,Map,ValuesNoLabel,[]),
	verbose('expand tree template ~w => values=~w\n',[Templates,ValuesNoLabel]),
	record_without_doublon(templates(Feats,ValuesNoLabel)),
	verbose('done templates\n',[]),
	true
	.
