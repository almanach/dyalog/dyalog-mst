/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  mst.pl -- Maximum Spanning Tree Algorithm
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').

:-extensional
  edge/4,
  node/1,
  entering/2,
  in/5,
  parent/2,
  prev/2,
  children/2,
  root/1
.

:-std_prolog find_mst.

find_mst :-
    true,
    %% find node and install reverse edges
%    format('install reverse edge\n',[]),
    every((
		 edge(S,T,L,W),
		 record_without_doublon(node(S)),
		 record_without_doublon(node(T))
	     )),
%    format('initialization\n',[]),
    every((
		 node(T),
		 entering!new(T,E),
		 every(( edge(S,T,L,W),
			 entering!add(E,S,T,L,W)
		       )),
		 true
	     )),
    %% select a starting node
    (root(N) xor fail),
%    format('starting with ~w\n',[N]),
    mst!contract(N),
    show_time(contract),
    mst!expand,
    true
.

:-std_prolog mst!contract/1.

mst!contract(A) :-
%    format('contract at ~w\n',[A]),
    ( entering!first(A,U,V,L,Max) ->
%      format('\tbest entering ~w ~w ~w\n',[U,V,Max]),
      mst!parent(U,B),
%      format('\tbest entering ~w ~w (~w) ancestor(~w)=~w\n',[U,V,Max,U,B]),
      ( A == B ->
	% do nothing
%	format('\t\tloop\n',[]),
	mst!contract(A)
      ;
      %edge(U,V,_Max),
      record(in(A,U,V,L,Max)),
%      format('\tadd in ~w: ~w -~w-> ~w (~w)\n',[A,U,L,V,Max]),
      ( in(B,_,_,_,_) ->
	  %% cycle
	  mst!new_node(C),
	  once_xor_true((
			       prev(B,X),
			       erase(prev(B,_)),
			       record(prev(C,X))
			   )),
	  record(prev(B,A)),
%	  format('\t\tcycle => new node ~w\n',[C]),
	  mst!cycle_process(C,B)
	;
	%% extend path
%	format('\t\textend to ~w\n',[B]),
	record(prev(B,A)),
	mst!contract(B)
	)
      )
    ;
    true
    ),
%    format('done contract at ~w\n',[A]),
    true
.

:-std_prolog mst!cycle_process/2.

mst!cycle_process(C,A) :-
%    format('cycle process ~w ~w\n',[C,A]),
    (parent(A,_) ->
	 %% end of cycle
%	 format('\t\tend of cycle ~w at ~w\n',[C,A]),
	 mst!contract(C)
    ;
    record(parent(A,C)),
%    format('\t\t\tadd children ~w ~w\n',[C,A]),
    record(children(C,A)),
    (in(A,_,_,_,WIn) xor WIn=0),
    entering(C,E),
    entering(A,EA),
    entering!merge(E,EA,WIn),
    erase(entering(A,_)),
    (prev(A,B) xor fail),
    mst!cycle_process(C,B)
    )
    .

:-std_prolog mst!entering/4.

mst!entering(A,U,V,L,W) :-
    entering(A,U,V,L,_W),
    mst!weight(U,V,L,W),
    true
    .

:-std_prolog mst!weight/3.

mst!weight(U,V,L,W) :-
    edge(U,V,L,W0),
    mst!delta(V,Delta),
    W is W0 - Delta
.

:-std_prolog mst!delta/2.

mst!delta(A,Delta) :-
    ( parent(A,B) ->
      in(A,_,_,_,Delta0),
      mst!delta(B,Delta1),
      Delta is Delta0 + Delta1
    ;
    Delta = 0
    )
.
    
:-std_prolog mst!new_node/1.

mst!new_node(N::s(C)) :-
    update_counter(snode,C),
    entering!new(N,E),
    record(node(N))
.

:-std_prolog mst!parent/2.

mst!parent(N,M) :-
    ( parent(N,_M) ->
      mst!parent(_M,M)
    ;
    M=N
    )
.


:-light_tabular mst!expand/0.

mst!expand :-
    root(R),
    (node(S::s(_)),
     \+ parent(S,_),
     (in(S,_U,_V,_L,_W) ->
	  format('max cluster is ~w with in ~w  -~w-> ~w (~w)',[S,_U,_L,_V,_W])
     ;
     format('max cluster is ~w with no in\n',[S])
     )
    ),
    record(in(S,-,R,noop,-10000)),
    mst!uncontract(S)
.

:-std_prolog mst!uncontract/1.

mst!uncontract(A) :-
%    format('uncontract ~w\n',[A]),
    in(A,U,V,L,W),
%    format('\tin ~w-~w->~w\n',[U,L,V]),
    mst!child_ancestor(A,V,B),
%    format('\tchild ancestor ~w\n',[B]),
    erase(in(B,_,_,_,_)),
    record(in(B,U,V,L,W)),
    every(( children(A,_B),
%	    format('\tchildren ~w\n',[_B]),
	    mst!uncontract(_B)
	  ))
.

:-light_tabular mst!child_ancestor/3.
:-mode(mst!child_ancestor/3,+(+,+,-)).

mst!child_ancestor(A,V,B) :-
    children(A,B),
    (B = V xor mst!child_ancestor(B,V,_))
    .

:-xcompiler
entering!new(N,E) :-
    '$interface'(entering_new(),[return(E:ptr)]),
    record(entering(N,E))
    .

:-xcompiler
entering!add(E,U,V,L,W) :-  '$interface'(entering_add(E:ptr,U:term,V:term,L:term,W:int),[return(none)]).

:-xcompiler
entering!merge(E1,E2,Delta) :- '$interface'(entering_merge(E1:ptr,E2:ptr,Delta:int),[return(none)]).

:-xcompiler
entering!first(A,U,V,L,W) :-
     entering(A,E),    
    '$interface'(entering_first(E:ptr,U:term,V:term,L:term,W:term),[]).  




