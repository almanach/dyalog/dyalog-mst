/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2013, 2014, 2015, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  engine_c.c 
 *  \brief C support for engine.pl
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <regex.h>
#include <math.h>
#include <ctype.h>

#undef PACKAGE_VERSION
#undef PACKAGE_TARNAME
#undef PACKAGE_STRING
#undef PACKAGE_NAME
#undef VERSION
#undef PACKAGE

#include "libdyalog.h"
#include "builtins.h"

extern char *getenv();


#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (s, n)
     const char *s;
     size_t n;
{
  size_t len = my_strnlen (s, n);
  char *new = malloc (len + 1);

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}

#endif



static fol_t
sfol_full_copy( fol_t t, fkey_t Sk(t) )
{
    Deref(t);

    if (FOL_GROUNDP(t)) {
        return t;
    } else if (FOL_DEREFP(t)) {
        dyalog_printf("** pb variable in sfol_full_copy %&f\n",t);
        return t;
    } else {
        unsigned long arity = FOLCMP_ARITY( t );
        fol_t *arg = &FOLCMP_REF(t,1);
        fol_t *stop = arg + arity;

        FOLCMP_WRITE_START( FOLCMP_FUNCTOR( t ), arity );

        for (; arg < stop ;)
            FOLCMP_WRITE( sfol_full_copy( *arg++, Sk(t)) );

        return FOLCMP_WRITE_STOP;
    }
}

Bool
DyALog_Copy( sfol_t src, sfol_t dest )
{
    return Unify(dest->t,dest->k,sfol_full_copy(src->t,src->k),Key0);
}

typedef struct entering 
{
    fol_t source;
    fol_t target;
    fol_t label;
    long weight;
    struct entering *next;
} * entering_t;

entering_t
entering_new () 
{
    entering_t first = (entering_t) GC_MALLOC(sizeof(struct entering));
    first->target = NULL;
    first->source = NULL;
    first->label = NULL;
    first->weight = 0;
    return first;
}
    
void
entering_add (entering_t first, sfol_t source, sfol_t target, sfol_t label, long weight) 
{
    entering_t cell = (entering_t) GC_MALLOC(sizeof(struct entering));
    entering_t *current = &(first->next);
    SFOL_Deref(source);
    SFOL_Deref(target);
    SFOL_Deref(label);
    cell->source = source->t;
    cell->target = target->t;
    cell->label = label->t;
    cell->weight = weight;
    for(; *current && (*current)->weight > weight; current = &((*current)->next));
    cell->next = *current;
    *current = cell;
}

void
entering_merge (entering_t firstA, entering_t firstB, long delta) 
{
    entering_t *current = &(firstA->next);
    entering_t p = firstB->next;
    entering_t next=NULL;
    for(; p; ) {
        p->weight -= delta;
        for(; *current && (*current)->weight > p->weight; current = &((*current)->next));
        next = p->next;
        p->next = *current;
        *current = p;
        p = next;
        current = &((*current)->next);
    }
}

Bool
entering_first(entering_t e, sfol_t source, sfol_t target, sfol_t label, sfol_t weight) 
{
    entering_t cell = e->next;
    if (!cell)
        Fail;
    e->next = cell->next;
    return Unify(cell->source,Key0,source->t,source->k)
        && Unify(cell->target,Key0,target->t,target->k)
        && Unify(cell->label,Key0,label->t,label->k)
        && Unify(DFOLINT(cell->weight),Key0,weight->t,weight->k)
        ;
}




    
