/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  engine.pl -- A Dyalog based Maximum Spanning Tree parser
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').
:-require('mst.pl').
:-require('features.pl').

:-extensional info!comment/1.
:-extensional info!contracted/3.
:-extensional info!ellipse/3.

:-extensional oracle!end.

:-extensional oracle!edge/3.
:-extensional oracle!redge/3.

:-extensional tagset!constraint/3.
:-extensional tagset!root/1.

:-extensional
  tagset!ensure_tree/0		% single root
  .

:-extensional
  root_edge/3
  .

:-extensional
  xedge/4,
  xroot/2
  .

?- %fail,
   verbose('new round\n',[]),
   show_time(latency),
   process_options,
   ( oracle!end ->
     (opt(save_model(Model)) ->
	  model!save(Model)
     ; opt(save_binary_model(Model)) ->
       model!save_binary(Model)
     ;
     true
     ),
     show_time(model_save)
   ;
     show_time(model_load),
     parse
   ),
   fail
   .

process_options :-
        ( recorded(parsed_options)
        xor
        argv(Options),
          parse_options(Options),
	  generate_templates,
	  record(parsed_options),
	  model!init
	),
%	verbose('adding persistent\n',[]),
	persistent!add(parsed_options),
	persistent!add_fact(opt(_)),
	persistent!add_fact(templates(_,_,_)),
	persistent!add_fact(templates(_,_)),
	persistent!add_fact(tagset!label(_)),
	persistent!add_fact(tagset!root(_)),
	persistent!add_fact(tagset!ensure_tree),
	persistent!add_fact(guide!xaction(_,_,_)),
	persistent!add_fact(tagset!outside_bound(_)),
	verbose('processed options\n',[]),
	true
	.

:-std_prolog parse_options/1.

parse_options(Options) :-
        ( Options == [] -> true
        ;
	( Options = ['-verbose'|Rest] -> record_without_doublon( opt(verbose) )
	; Options = ['-multi'|Rest] -> record_without_doublon( opt(multi) )
	; Options = ['-res',File|Rest] -> read_dbfile(File)
	; Options = ['-train'|Rest] ->
	  %% default training mode
	  erase(opt(train:_)),
	  record_without_doublon(opt(train:std))
	; Options = ['-load_model',Model|Rest] ->
	  record_without_doublon(opt(load_model(Model))),
	  model!load(Model),
	  true
	; Options = ['-load_binary_model',Model|Rest] ->
	  record_without_doublon(opt(load_model(Model))),
	  format('load binary model ~w\n',[Model]),
	  model!load_binary(Model),
	  true
	; Options = ['-save_model',Model|Rest] ->
          record_without_doublon(opt(save_model(Model))),
          true
	; Options = ['-save_binary_model',Model|Rest] ->
          record_without_doublon(opt(save_binary_model(Model))),
          true
	; Options = [ParserOption|Rest]
	),
	parse_options(Rest)
        )
.

:-xcompiler
persistent!add_fact(Fact) :-
	every(( recorded(Fact),
		persistent!add(Fact),
		true
	      ))
.

:-xcompiler
persistent!add(Fact) :-
	'$interface'('DyALog_Persistent_Add'(Fact:term),[return(none)]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parsing

%:-std_prolog parse/0.

parse :-
    'N'(N),
    verbose('processing new sentence N=~w\n',[N]),
    ( opt(train:_) ->
      %% we only advance the perceptron in training mode
      model!advance,
      true
    ;
    opt(check) xor  abolish(oracle!action/2)
    ),
    ('S'(SId) xor SId='E1'),
    %% prepare graph
    build_fcat_at(0),
    mutable(NEdges,0),
    (tagset!ensure_tree
     xor
     record(root(0)),
     record(xroot(0,0))
    ),
    templates(Features,_Label,Values),
    every((
		 ( 'C'(Left1,Id1,Lemma1::lemma{ fullcat => FCat1},Right1),
		   record('rC'(Right1,Id1,Lemma1,Left1)),
		   (Id1 = XId1 + _ xor Id1 = XId1),
		   (tagset!ensure_tree xor
		    record(edge(XId1,0,noop,-50000)), % pseudo edge from word to root
		    mutable_add(NEdges,1)
		   )
		 ; Id1 = 0,
		   XId1 = 0,
		   Left1 = 0 % Root node
		 ),
		 every((
			      'C'(Left2,Id2,Lemma2::lemma{ fullcat => FCat2 },Right2),
			      (Id2 = XId2 + _ xor Id2 = XId2),
			      Id1 \== Id2,
			      XId1 \== XId2,
			      ( Id1 \== 0,
				tagset!constraint(FCat2,FCat1,MaxDistance) ->
				abs(Left2-Left1) < MaxDistance
			      ;
			      true
			      ),
			      verbose('try install edge ~w/~w ~w/~w\n',[Id1,XId1,Id2,XId2]),
			      mutable(MWeight,-50000),
			      mutable(MLabel,noop),
			      every((
					   edge_features(Left1,Id1,Left2,Id2,Features),
					   % verbose('try2 install edge ~w ~w\n',[Id1,Id2]),
					   model!query(Values,_Weight,[]),
					   mutable_check_max(MWeight,_Weight),
					   mutable(MWeight,_Weight),
					   mutable(MLabel,_Label)
				       )),
			      mutable_read(MLabel,Label),
			      mutable_read(MWeight,Weight),
			      ( Id1 == 0,
				tagset!ensure_tree
			      ->
			      (root(_XId2),
			       root_edge(_XId2,_OldLabel,_OldWeight),
			       _OldWeight > Weight ->
				   %% _Id2 looks like a better root than Id2: we keept _Id2
				   true
				;
				%% we install Id2 as the new current root
				erase(root(_)),
				erase(xroot(_,_)),
				erase(root_edge(_,_,_)),
				record(root_edge(XId2,Label,Weight)),
				record(root(XId2)),
				record(xroot(XId2,Id2))
			      )
			      ;
			      edge(XId1,XId2,_OldLabel,_OldWeight),
			      _OldWeight > Weight ->
			      true
			      ;
			      erase(edge(XId1,XId2,_,_)),
			      erase(xedge(XId1,XId2,_,_)),
			      record(edge(XId1,XId2,Label,Weight)),
			      record(xedge(XId1,XId2,Id1,Id2)),
			      mutable_add(NEdges,1)
			      )
			  )), 
		 true
	     )),
    show_time(graphinit),
    (N = 1,
     tagset!ensure_tree ->
	 %% no need for mst when there is a single node (the root !)
	 true
    ; find_mst ->
      true
    ;
    format('## no mst found for sid=~w\n',[SId]),
    every(( root(R),
	    format('## root ~w\n',[R]),
	    edge(ZA,ZB,ZL,ZW),
	    format('## edge ~w -[~w]-> ~w (~w)\n',[ZA,ZL,ZB,ZW]),
	    true
	  )),
    fail
    ),
    show_time(parsing),
    mutable_read(NEdges,Edges),
    format('## ~w edges=~w\n',[SId,Edges]),
    every((info!comment(Comment),
	   format('##. ~w\n',[Comment])
	  )),
    every((info!ellipse(0,Beta,CForm),
	   format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [0,Beta,CForm,'_','_','_','_','_','_','_'] )
	  )),
    parse_display(0,0),
    (opt(train:_) ->
	 verbose('training phase\n',[]),
	 every(( oracle!edge(GoldHead,V,GoldLabel),
		 record(oracle!redge(V,GoldHead,GoldLabel))
	       )),
	 mutable(MScore,0),
	 every((
		      templates(PredFeatures,PredLabel,PredValues),
		      templates(GoldFeatures,GoldLabel,GoldValues),
		      templates(PredFeatures,PredValuesNoLabel),
		      templates(GoldFeatures,GoldValuesNoLabel),
		      root(R),
		      ( in(V,PredHead,V,PredLabel,_),
			V \== R,
			edge(PredHead,V,PredLabel,PredW),
			xedge(PredHead,V,XPredHead,XPredV)
		      ;
		        V = R,
			root_edge(V,PredLabel,PredW),
			xroot(V,XPredV),
			PredHead = 0,
			XPredHead = 0,
%			oracle!edge(0,_V,_Label),
%			_V \== V,
			true
		      ),
		      (oracle!redge(V,XGoldHead,GoldLabel), XGoldV=V xor oracle!redge(V+VCat,XGoldHead,GoldLabel), XGoldV=V+VCat),
		      (XGoldHead = GoldHead + _ xor XGoldHead = GoldHead),
		      \+ ( PredLabel == GoldLabel,
			   PredHead == GoldHead,
			   mutable_inc(MScore,1),
			   true
			 ),
		      verbose('udpdating head info predicted=~w-~w->~w gold=~w-~w->~w\n',
			      [PredHead,PredLabel,V,GoldHead,GoldLabel,V]),
		      
		      'C'(LeftV,XPredV,_,_),

/*
		      (edge(GoldHead,V,GoldLabel,GoldW)
		       xor edge_features(LeftGoldHead,GoldHead,LeftV,V,GoldFeatures),
		       templates(GoldFeatures,GoldLabel,GoldValues),
		       model!query(GoldValues,GoldW,[])
		       xor GoldW = 0
		      ),
		      PDelta is max(1,min(abs(GoldW-PredW)/10,100)),
*/
		      PDelta is 1,
		      MDelta is -PDelta,
		      (PredHead = 0 -> LeftPredHead=0 ; 'C'(LeftPredHead,PredHead,_,_)),
		      edge_features(LeftPredHead,PredHead,LeftV,XPredV,PredFeatures),
		      (GoldHead = 0 -> LeftGoldHead = 0 ; 'C'(LeftGoldHead,GoldHead,_,_)),
		      edge_features(LeftGoldHead,XGoldHead,LeftV,XGoldV,GoldFeatures),
		      once_xor_true((%%PredLabel \== GoldLabel,
				     %%  update for bad label
				     model!update(PredValues,MDelta),
				     verbose('done pred update2 values=~w\n',[PredLabel]),
				     model!update(GoldValues,PDelta),
				     verbose('done gold update values=~w\n',[GoldLabel]),
				     true
				    )),
		      once_xor_true((PredHead \== GoldHead,
				     %%  update for bad attachement
				     model!update(PredValuesNoLabel,MDelta),
				     verbose('done pred attach update2 values=~w\n',[PredLabel]),
				     model!update(GoldValuesNoLabel,PDelta),
				     verbose('done gold attach update values=~w\n',[GoldLabel]),
				     true
				    )),
		      true
		  )),
	 mutable_read(MScore,Score),
	 Ratio is 100.0 * Score / N,
	 format('## score=~w/~w ratio=~w\n',[Score,N,Ratio]),
	 %%model!save('toto.mdl'),
	 show_time(training),
	 true
    ;
    true
    )
.

:-std_prolog parse_display/2.

parse_display(Left,Total) :-
%    format('display left=~w\n',[Left]),
    'C'(Left,XId,lemma{ lex => Lex, lemma => Lemma, fullcat => FCat, cat => Cat, mstag => FS },Right),
    %format('display1 left=~w id=~w\n',[Left,Id]),
    (XId = Id + _ xor XId = Id),
    (in(Id,Gov,Id,Label,W), 
     edge(Gov,Id,Label,W0),
     xedge(Gov,Id,XGov,XId)
    ->
	 true
    ;
    root(Id),
    xroot(Id,XId),
    root_edge(Id,Label,W0),
    Gov = 0
    ),
    NewTotal is Total+W0,
    every((info!contracted(Id,XRight,CForm),
	   format('~w-~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [Id,XRight,CForm,'_','_','_','_','_','_','_'] )
	  )),
    every((info!ellipse(Id,Beta,CForm),
	   format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [Id,Beta,CForm,'_','_','_','_','_','_','_'] )
	  )),
    %format('got ~w => ~w ~w ~w\n',[Id,_Gov,Label,W]),
    (FS = [_|_] -> XFS = FS ; XFS = [FS]),
    format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [Id,Lex,Lemma,Cat,FCat,['~w','|'],XFS,Gov,Label,W0] ),
    %format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [Id,Lex,Lemma,Cat,FCat,['~w','|'],XFS2,Gov,Label,Id] ),
    (parse_display(Right,NewTotal) xor format('\n\n## Cost ~w\n',[NewTotal]))
    .

:-xcompiler
model!init :- '$interface'(mdl_init(),[return(none)]).

:-xcompiler
model!advance :- '$interface'(mdl_perceptron_advance(),[return(none)]).

%%tagset!constraint('DET',_,4).
%%Tagset!constraint('CLS',_,6).
%%tagset!constraint('CLO',_,4).
%%tagset!constraint('CLR',_,4).

:-xcompiler
model!save_binary(Model) :- '$interface'('Mdl_Save_Binary'(Model:string),[return(none)]).
